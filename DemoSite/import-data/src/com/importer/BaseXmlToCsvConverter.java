package com.importer;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.xml.parsers.SAXParser;
import javax.xml.parsers.SAXParserFactory;

import org.apache.commons.io.FileUtils;
import org.xml.sax.Attributes;
import org.xml.sax.SAXException;
import org.xml.sax.helpers.DefaultHandler;

import com.opencsv.CSVWriter;

/**
 * Converts a Merchant Center XML Feed into a CSV file.
 * 
 */
public class BaseXmlToCsvConverter extends DefaultHandler {
    // Specify the list of fields you want here:
    private final List<String> requiredFields = Arrays.asList("g:ID", "link", "title", "description", "g:mpn", "g:availability", "g:google_product_category", "g:image_link", "g:condition", "g:price", "g:product_type");

    private final String filename;
    private final CSVWriter writer;
    private static List<Map<String, String>> allData = new ArrayList<Map<String,String>>();
    private static  Map<String, String> dataRow = new HashMap<String, String>();
    private String currentElement;
    private StringBuilder textBuffer;

     
  
  

    public BaseXmlToCsvConverter(String filename) throws IOException {
	this.filename = filename;
	this.writer = new CSVWriter(new FileWriter(filename + ".csv"));
	List<String> header = new ArrayList<String>();
	header.addAll(requiredFields);
	for (int i = 0; i < 20; i++) {
	    header.add("type" + i);
	}
	writer.writeNext(header.toArray(new String[] {}));
	writer.flush();
	 
    }

    public void convert() throws Exception {
	DefaultHandler handler = new BaseXmlToCsvConverter(filename);
	SAXParserFactory factory = SAXParserFactory.newInstance();
	SAXParser saxParser = factory.newSAXParser();
	File file = new File(filename);
	saxParser.parse(file, handler);
    }

    boolean insideShipping = false;

    public void startElement(String namespaceUri, String localName, String qualifiedName, Attributes attrs) throws SAXException {

	if (qualifiedName.equals("item")) {
	    insideShipping = false;
	    allData.add(new HashMap<String, String>(dataRow));
	    dataRow.clear();
	}

	if (qualifiedName.equals("g:shipping")) {
	    insideShipping = true;
	}

	if (requiredFields.contains(qualifiedName)) {
	    currentElement = qualifiedName;
	} else {
	    currentElement = null;
	}

	// Special case: the "link" element has its value stored in the "href"
	// attribute.
	if (qualifiedName.equals("link")) {
	    dataRow.put("link", attrs.getValue("href"));
	}

	textBuffer = new StringBuilder();
    }

    public void endElement(String namespaceUri, String localName, String qualifiedName) {

	if (!textBuffer.toString().equals("") && currentElement != null) {
	    if (currentElement.equals("g:price") && dataRow.get("g:price") != null) {

	    } else {
		String data = textBuffer.toString();
		  data = data.replace("\"\"", "");
		  data = data.replace("\n\r", "");
		  data = data.replace("\n", "");
		  data = data.replace("\r", "");
		  data = data.replace(",", " ");
		dataRow.put(currentElement, data);
	    }
	}

	if (qualifiedName.equals("item")) {
	    List<String> outputArray = new ArrayList<String>();
	    for (String requiredField : requiredFields) {
		outputArray.add(dataRow.get(requiredField));
	    }
	    String data = dataRow.get("g:product_type");
	    if (data != null) {
		String[] parts = data.split(">");
		for (String part : parts) {
		    outputArray.add(part.trim());
		}

	    }
	    try {
		System.out.println(outputArray.toArray(new String[] {}));
		writer.writeNext(outputArray.toArray(new String[] {}));
		writer.flush();
	    } catch (IOException e) {
		throw new RuntimeException(e);
	    }
	}
    }

    public void characters(char buf[], int start, int length) throws SAXException {
	for (int i = start; i < start + length; i++) {
	    textBuffer.append(buf[i]);
	}
    }
}
