package com.importer;

public class DataBean {

	
	private String pde;
	private String mnpn;
	private String size;
	private String supplier;
	private String promo;
	private String heading;
	private String description;
	private String images;
	private String disclaimers;
	public String getPde() {
		return pde;
	}
	public void setPde(String pde) {
		this.pde = pde;
	}
	public String getMnpn() {
		return mnpn;
	}
	public void setMnpn(String mnpn) {
		this.mnpn = mnpn;
	}
	public String getSize() {
		return size;
	}
	public void setSize(String size) {
		this.size = size;
	}
	public String getSupplier() {
		return supplier;
	}
	@Override
	public String toString() {
		return "DataBean [pde=" + pde + ", mnpn=" + mnpn + ", size=" + size + ", supplier=" + supplier + ", promo=" + promo + ", heading=" + heading + ", description=" + description + ", images=" + images + ", disclaimers=" + disclaimers + "]";
	}
	public void setSupplier(String supplier) {
		this.supplier = supplier;
	}
	public String getPromo() {
		return promo;
	}
	public void setPromo(String promo) {
		this.promo = promo;
	}
	public String getHeading() {
		return heading;
	}
	public void setHeading(String heading) {
		this.heading = heading;
	}
	public String getDescription() {
		return description;
	}
	public void setDescription(String description) {
		this.description = description;
	}
	public String getImages() {
		return images;
	}
	public void setImages(String images) {
		this.images = images;
	}
	public String getDisclaimers() {
		return disclaimers;
	}
	public void setDisclaimers(String disclaimers) {
		this.disclaimers = disclaimers;
	} 
		
}
