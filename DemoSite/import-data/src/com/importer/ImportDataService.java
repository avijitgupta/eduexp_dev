//package com.importer;
//
//import java.io.File;
//import java.util.ArrayList;
//import java.util.List;
//
//import org.apache.commons.io.FileUtils;
//
//import atg.dtm.TransactionDemarcation;
//import atg.epub.ProgramaticImportService;
//import atg.epub.project.Process;
//import atg.repository.MutableRepository;
//import atg.repository.MutableRepositoryItem;
//import atg.repository.RepositoryItem;
//import atg.repository.RepositoryView;
//import atg.repository.rql.RqlStatement;
//
//public  class ImportDataService extends ProgramaticImportService {
//
//	private MutableRepository productCatalog;
//
//	  
//	public MutableRepository getProductCatalog() {
//		return productCatalog;
//	}
//
//	public void setProductCatalog(MutableRepository productCatalog) {
//		this.productCatalog = productCatalog;
//	}
//
//	public void importUserData(Process pProcess, TransactionDemarcation pTD) throws Exception {
//		List<DataBean> dataBeans = getDataBeans();
//		for (DataBean dataBean : dataBeans) {
//			 MutableRepositoryItem product =  findItem(  dataBean) ;
//			 if(product == null) {
//				 product = productCatalog.createItem(dataBean.getImages(),"product");
//			 }
//			 String imageName = dataBean.getImages();
//			 System.out.println("mv " + imageName + ".jpg " + product.getRepositoryId() + ".jpg");
//			 
//			 product.setPropertyValue("size", dataBean.getSize());
//			 product.setPropertyValue("pharm", "TRUE");
//			 product.setPropertyValue("descriptionDefault", dataBean.getDescription());
//			 product.setPropertyValue("warnings", dataBean.getDisclaimers());
//			 product.setPropertyValue("displayNameDefault", dataBean.getHeading());
//		} 
//
//	}
//
//	private MutableRepositoryItem findItemByPde(String pde) throws Exception {
//
//		RepositoryView rview = productCatalog.getView("product");
//		RqlStatement statement = RqlStatement.parseRqlStatement("pde = ?0");
//		Object params[] = new Object[1];
//		params[0] = new String(pde);
//		RepositoryItem[] items = statement.executeQuery(rview, params);
//		if (items != null && items.length != 0) {  
//			RepositoryItem ritem = items[0];
//			return (MutableRepositoryItem) ritem;
//		}
//		return null;
//	}
//
//	private MutableRepositoryItem findItemByMnpn(String mnpn) throws Exception {
//
//		RepositoryView rview = productCatalog.getView("product");
//		RqlStatement statement = RqlStatement.parseRqlStatement("mnpn = ?0");
//		Object params[] = new Object[1];
//		params[0] = new String(mnpn);
//		RepositoryItem[] items = statement.executeQuery(rview, params);
//		if (items != null && items.length != 0) {  
//			RepositoryItem ritem = items[0];
//			return (MutableRepositoryItem) ritem;
//		}
//		return null;
//	}
//
//	private MutableRepositoryItem findItem(DataBean dataBean) {
//		MutableRepositoryItem item = null;
//		try {
//			item = findItemByPde(dataBean.getPde());
//			if (item != null) { 
//				return item;
//			}
//			item = findItemByMnpn(dataBean.getMnpn());
//			if (item != null) { 
//				return item;
//			}
//			item =  (MutableRepositoryItem)productCatalog.getItem(dataBean.getImages(), "product");;
//			if (item != null) { 
//				return item;
//			}
//		} catch (Exception e) {
//			e.printStackTrace();
//		}
//		return item;
//	}
//
//	private String dataFile = "/app/oracle/product/atg/data/chemart-product-data.txt";
//
//	private List<DataBean> getDataBeans() {
//		List<DataBean> beans = null;
//		try {
//			List<String> parts = getParts();
//			beans = new ArrayList<DataBean>();
//			for (String part : parts) {
//				DataBean bean = createBean(part);
//				if (bean.getPde().isEmpty() && bean.getMnpn().isEmpty()) {
//
//				} else {
//					beans.add(bean);
//				}
//			}
//		} catch (Exception e) {
//			e.printStackTrace();
//		}
//		return beans;
//	}
//
//	private List<String> getParts() {
//		List<String> allParts = null;
//		try {
//			allParts = new ArrayList<String>();
//			String data = FileUtils.readFileToString(new File(dataFile));
//			String[] parts = data.split("###");
//			for (int i = 0; i < parts.length; i++) {
//				String part = parts[i];
//				allParts.add(part);
//			}
//		} catch (Exception e) {
//			e.printStackTrace();
//		}
//		return allParts;
//	}
//
//	private DataBean createBean(String data) {
//		DataBean bean = new DataBean();
//		String[] parts = data.split("\n");
//		for (int i = 0; i < parts.length; i++) {
//			String part = "";
//			try {
//				part = parts[i].split(":")[1].trim();
//			} catch (Exception e) {
//			}
//
//			if (i == 0) {
//				bean.setPde(part);
//			}
//			if (i == 1) {
//				bean.setMnpn(part);
//			}
//			if (i == 2) {
//				bean.setSize(part);
//			}
//			if (i == 3) {
//				bean.setSupplier(part);
//			}
//			if (i == 4) {
//				bean.setPromo(part);
//			}
//			if (i == 5) {
//				bean.setHeading(part);
//			}
//			if (i == 6) {
//				bean.setDescription(part);
//			}
//			if (i == 7) {
//				bean.setImages(part);
//			}
//			if (i == 8) {
//				bean.setDisclaimers(part);
//			}
//		}
//		return bean;
//	}
//
//}
