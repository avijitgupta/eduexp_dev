package com.importer;
import java.io.File;
import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Unmarshaller;
import org.sitemaps.*;


	public class JAXBExample {
		public static void main(String[] args) {
	 
		 try {
	 
			File file = new File("/app/oracle/product/atg/ATG/ImportData/build/sitemap-products.xml");
			JAXBContext jaxbContext = JAXBContext.newInstance(Urlset.class);
	 
			Unmarshaller jaxbUnmarshaller = jaxbContext.createUnmarshaller();
			Urlset urlset = (Urlset) jaxbUnmarshaller.unmarshal(file);
			System.out.println(urlset);
	 
		  } catch (JAXBException e) {
			e.printStackTrace();
		  }
	 
		}
	 
}
