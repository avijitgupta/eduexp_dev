//package com.importer;
//
//import java.util.ArrayList;
//import java.util.HashSet;
//import java.util.Iterator;
//import java.util.List;
//import java.util.Set;
//
//import atg.dtm.TransactionDemarcation;
//import atg.epub.ProgramaticImportService;
//import atg.epub.project.Process;
//import atg.repository.MutableRepository;
//import atg.repository.MutableRepositoryItem;
//import atg.repository.RepositoryException;
//import atg.repository.RepositoryItem;
//import atg.repository.RepositoryView;
//import atg.repository.rql.RqlStatement;
//
//@SuppressWarnings({ "rawtypes", "unchecked" })
//public class PricesToProductsService extends ProgramaticImportService {
//
//	private MutableRepository productCatalog;
//
//	public PricesToProductsService() {
//		System.setProperty("http.proxyHost", "au-proxy.au.oracle.com");
//		System.setProperty("http.proxyPort", "80");
//	}
//
//	public MutableRepository getProductCatalog() {
//		return productCatalog;
//	}
//
//	public void setProductCatalog(MutableRepository productCatalog) {
//		this.productCatalog = productCatalog;
//	}
//
//	public void importUserData(Process pProcess, TransactionDemarcation pTD) throws Exception {
//		List<MutableRepositoryItem> allPharmProducts = findAllPharm();
//		for (MutableRepositoryItem product : allPharmProducts) {
//			String skuId = "sku" + product.getRepositoryId();
//			MutableRepositoryItem sku = createOrGetSkuItem(skuId);
//			String p1 = (String) product.getPropertyValue("p1");
//			String p2 = (String) product.getPropertyValue("p2");
//			Double listPrice = getPrice(p1);
//			Double salePrice = getPrice(p2);
//
//			sku.setPropertyValue("listPrice", listPrice);
//			sku.setPropertyValue("salePrice", salePrice);
//			sku.setPropertyValue("onSale", Boolean.TRUE);
//			sku.setPropertyValue("displayNameDefault", product.getPropertyValue("displayNameDefault"));
//
//			Set<MutableRepositoryItem> parentProducts = new HashSet<MutableRepositoryItem>();
//			parentProducts.add(product);
//			sku.setPropertyValue("parentProducts", parentProducts);
//
//			productCatalog.updateItem(sku);
//
//			List<MutableRepositoryItem> childSKUs = new ArrayList<MutableRepositoryItem>();
//			childSKUs.add(sku);
//			product.setPropertyValue("childSKUs", childSKUs);
//
//			productCatalog.updateItem(product);
//		}
//	}
//
//	private MutableRepositoryItem createOrGetSkuItem(String id) throws RepositoryException {
//		MutableRepositoryItem sku = (MutableRepositoryItem) productCatalog.getItem(id, "sku");
//		if (sku == null) {
//			sku = productCatalog.createItem(id, "sku");
//		}
//		return sku;
//	}
//
//	private Double getPrice(String price) {
//		Double result = 0d;
//		if (price == null || price.isEmpty()) {
//			return result;
//		}
//		price = price.replaceAll("Was", "");
//		price = price.replaceAll("RRP", "");
//		price = price.replaceAll("AUD", "");
//		price = price.replaceAll("\\$", "");
//		price = price.trim();
//		try {
//			result = Double.parseDouble(price);
//		} catch (Exception e) {
//			e.printStackTrace();
//		}
//		return result;
//	}
//
//	private List<MutableRepositoryItem> findAllPharm() throws Exception {
//
//		List<MutableRepositoryItem> pharms = new ArrayList<MutableRepositoryItem>();
//		RepositoryView rview = productCatalog.getView("product");
//		RqlStatement statement = RqlStatement.parseRqlStatement("pharm = ?0");
//		Object params[] = new Object[1];
//		params[0] = new String("TRUE");
//		RepositoryItem[] items = statement.executeQuery(rview, params);
//		if (items != null && items.length != 0) {
//			for (int i = 0; i < items.length; i++) {
//				pharms.add((MutableRepositoryItem) items[i]);
//			}
//		}
//		return pharms;
//	}
//
//}
