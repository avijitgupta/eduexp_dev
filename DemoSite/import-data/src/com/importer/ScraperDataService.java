//package com.importer;
//
//import java.io.File;
//import java.net.URL;
//import java.util.ArrayList;
//import java.util.List;
//import java.util.concurrent.TimeUnit;
//import java.util.regex.Matcher;
//import java.util.regex.Pattern;
//
//import org.apache.commons.io.FileUtils;
//import org.openqa.selenium.By;
//import org.openqa.selenium.WebDriver;
//import org.openqa.selenium.htmlunit.HtmlUnitDriver;
//
//import atg.dtm.TransactionDemarcation;
//import atg.epub.ProgramaticImportService;
//import atg.epub.project.Process;
//import atg.repository.MutableRepository;
//import atg.repository.MutableRepositoryItem;
//import atg.repository.RepositoryItem;
//import atg.repository.RepositoryView;
//import atg.repository.rql.RqlStatement;
//
//public class ScraperDataService extends ProgramaticImportService {
//
//	private MutableRepository productCatalog;
//
//	public ScraperDataService() {
//		System.setProperty("http.proxyHost", "au-proxy.au.oracle.com");
//		System.setProperty("http.proxyPort", "80");
//		System.out.println("XXXXXXXXX");
//
//	}
//
//	public MutableRepository getProductCatalog() {
//		return productCatalog;
//	}
//
//	public void setProductCatalog(MutableRepository productCatalog) {
//		this.productCatalog = productCatalog;
//	}
//
//	public void importUserData(Process pProcess, TransactionDemarcation pTD) throws Exception {
//		List<MutableRepositoryItem> allPharmProducts = findAllPharm();
//		for (MutableRepositoryItem product : allPharmProducts) {
//			updateProducts(product);
//		}
//	}
//
//	public void updateProducts(MutableRepositoryItem product) throws Exception {
//		try {
//			HtmlUnitDriver driver = new HtmlUnitDriver(false);
//			driver.setProxy("au-proxy.au.oracle.com", 80);
//			driver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);
//
//			String repositoryId = product.getRepositoryId();
//			System.out.println("processing " + repositoryId);
//
//			driver.get("http://www.avenuespharmacy.com.au/list.php?search=" + repositoryId);
//			String pageSource = driver.getPageSource();
//			if (pageSource != null && pageSource.contains("No match found")) {
//				return;
//			}
//			if (pageSource.contains("More Information")) {
//
//				driver.findElement(By.linkText("More Information")).click();
//				pageSource = driver.getPageSource();
//
//				String breadcrumb = driver.findElement(By.cssSelector("div.breadcrumb")).getText();
//				String p1 = driver.findElement(By.cssSelector("div.list-was-price")).getText();
//				String p2 = driver.findElement(By.cssSelector("div.list-price")).getText();
//				String indications = driver.findElement(By.xpath("//table[2]/tbody/tr/td[2]")).getText();
//
//				product.setPropertyValue("breadcrumb", breadcrumb);
//				product.setPropertyValue("p1", p1);
//				product.setPropertyValue("p2", p2);
//				product.setPropertyValue("indications", indications);
//
//				Pattern pattern = Pattern.compile(".*<a href=\"(.*)\" rel=\"lytebox\"");
//
//				Matcher matcher = pattern.matcher(pageSource);
//				while (matcher.find()) {
//					String imageUrl = "http://www.avenuespharmacy.com.au" + matcher.group(1);
//
//					FileUtils.copyURLToFile(new URL(imageUrl), new File("/app/oracle/product/atg/data/" + repositoryId + ".jpg"));
//				}
//			}
//		} catch (Throwable e) {
//			e.printStackTrace();
//		}
//	}
//
//	private List<MutableRepositoryItem> findAllPharm() throws Exception {
//
//		List<MutableRepositoryItem> pharms = new ArrayList<MutableRepositoryItem>();
//		RepositoryView rview = productCatalog.getView("product");
//		RqlStatement statement = RqlStatement.parseRqlStatement("pharm = ?0");
//		Object params[] = new Object[1];
//		params[0] = new String("TRUE");
//		RepositoryItem[] items = statement.executeQuery(rview, params);
//		if (items != null && items.length != 0) {
//			for (int i = 0; i < items.length; i++) {
//				pharms.add((MutableRepositoryItem) items[i]);
//			}
//		}
//		return pharms;
//	}
//
//}
