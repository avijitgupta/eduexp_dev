//package com.importer;
//
//import java.util.ArrayList;
//import java.util.Iterator;
//import java.util.List;
//import java.util.Set;
//
//import atg.dtm.TransactionDemarcation;
//import atg.epub.ProgramaticImportService;
//import atg.epub.project.Process;
//import atg.repository.MutableRepository;
//import atg.repository.MutableRepositoryItem;
//import atg.repository.RepositoryItem;
//import atg.repository.RepositoryView;
//import atg.repository.rql.RqlStatement;
//
//@SuppressWarnings({ "rawtypes", "unchecked" })
//public class CopyOfCreateCategoriesService extends ProgramaticImportService {
//
//	private MutableRepository productCatalog;
//
//	public CopyOfCreateCategoriesService() {
//		System.setProperty("http.proxyHost", "au-proxy.au.oracle.com");
//		System.setProperty("http.proxyPort", "80");
//	}
//
//	public MutableRepository getProductCatalog() {
//		return productCatalog;
//	}
//
//	public void setProductCatalog(MutableRepository productCatalog) {
//		this.productCatalog = productCatalog;
//	}
//
//	public void importUserData(Process pProcess, TransactionDemarcation pTD) throws Exception {
//		List<MutableRepositoryItem> allPharmProducts = findAllPharm();
//		for (MutableRepositoryItem product : allPharmProducts) {
//			String p1 = (String) product.getPropertyValue("p1");
//
//			if (p1 != null && !p1.isEmpty()) {
//
//				String breadcrumb = (String) product.getPropertyValue("breadcrumb");
//				String splitChar = "\u00BB";
//
//				String[] catNames = breadcrumb.split(splitChar);
//				MutableRepositoryItem parentCategory = null;
//				for (int i = 0; i < catNames.length; i++) {
//					String catName = catNames[i].trim();
//					MutableRepositoryItem currentCategory = addOrUpdateCategory(catName);
//					if (parentCategory != null) {
//						addParentCategoryToCategory(currentCategory, parentCategory);
//						addChildCategoryToCategoryParent(currentCategory, parentCategory);
//					}
//
//					if (i == (catNames.length - 1)) {
//						addProductToCategory(currentCategory, product);
//					}
//					parentCategory = currentCategory;
//				}
//			}
//		}
//	}
//
//	private void addParentCategoryToCategory(MutableRepositoryItem currentCategory, MutableRepositoryItem parentCategory) throws Exception {
//		Set<MutableRepositoryItem> fixedParentCategories = (Set<MutableRepositoryItem>) currentCategory.getPropertyValue("fixedParentCategories");
//		boolean alreadyInList = false;
//		for (MutableRepositoryItem oneParentCategory : fixedParentCategories) {
//			String oneParentCategoryRepositoryId = oneParentCategory.getRepositoryId();
//			String newParentCategoryRepositoryId = parentCategory.getRepositoryId();
//			if (oneParentCategoryRepositoryId.equals(newParentCategoryRepositoryId)) {
//				alreadyInList = true;
//			}
//		}
//		if (alreadyInList) {
//			// its already in the list nothing to do
//		} else {
//			fixedParentCategories.add(parentCategory);
//			productCatalog.updateItem(currentCategory);
//		}
//
//	}
//
//	private void addChildCategoryToCategoryParent(MutableRepositoryItem currentCategory, MutableRepositoryItem parentCategory) throws Exception {
//		List<MutableRepositoryItem> fixedChildCategories = (List<MutableRepositoryItem>) parentCategory.getPropertyValue("fixedChildCategories");
//		boolean alreadyInList = false;
//		for (Iterator iterator = fixedChildCategories.iterator(); iterator.hasNext();) {
//			MutableRepositoryItem childCategory = (MutableRepositoryItem) iterator.next();
//			String childCategoryRepositoryId = childCategory.getRepositoryId();
//			String currentCategoryRepositoryId = currentCategory.getRepositoryId();
//			if (childCategoryRepositoryId.equals(currentCategoryRepositoryId)) {
//				alreadyInList = true;
//			}
//		}
//		if (alreadyInList) {
//			// its already in the list nothing to do
//		} else {
//			fixedChildCategories.add(currentCategory);
//			productCatalog.updateItem(parentCategory);
//		}
//	}
//
//	private MutableRepositoryItem addOrUpdateCategory(String catName) throws Exception {
//		String id = "pharmCat" + catName.hashCode();
//		MutableRepositoryItem category = (MutableRepositoryItem) productCatalog.getItem(id, "category");
//		if (category == null) {
//			category = productCatalog.createItem(id, "category");
//			category.setPropertyValue("displayNameDefault", catName);
//			category = (MutableRepositoryItem) productCatalog.addItem(category);
//		}
//		return category;
//	}
//
//	private void addProductToCategory(MutableRepositoryItem category, MutableRepositoryItem product) throws Exception {
//		List fixedChildProducts = (List) category.getPropertyValue("fixedChildProducts");
//		fixedChildProducts.add(product);
//		productCatalog.updateItem(category);
//	}
//
//	private List<MutableRepositoryItem> findAllPharm() throws Exception {
//
//		List<MutableRepositoryItem> pharms = new ArrayList<MutableRepositoryItem>();
//		RepositoryView rview = productCatalog.getView("product");
//		RqlStatement statement = RqlStatement.parseRqlStatement("pharm = ?0");
//		Object params[] = new Object[1];
//		params[0] = new String("TRUE");
//		RepositoryItem[] items = statement.executeQuery(rview, params);
//		if (items != null && items.length != 0) {
//			for (int i = 0; i < items.length; i++) {
//				pharms.add((MutableRepositoryItem) items[i]);
//			}
//		}
//		return pharms;
//	}
//
//}
