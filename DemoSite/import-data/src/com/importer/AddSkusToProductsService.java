//package com.importer;
//
//import java.math.BigDecimal;
//import java.util.ArrayList;
//import java.util.HashSet;
//import java.util.List;
//import java.util.Random;
//import java.util.Set;
//
//import atg.dtm.TransactionDemarcation;
//import atg.epub.ProgramaticImportService;
//import atg.epub.project.Process;
//import atg.repository.MutableRepository;
//import atg.repository.MutableRepositoryItem;
//import atg.repository.RepositoryException;
//import atg.repository.RepositoryItem;
//import atg.repository.RepositoryView;
//import atg.repository.rql.RqlStatement;
//
//@SuppressWarnings({ "rawtypes", "unchecked" })
//public class AddSkusToProductsService extends ProgramaticImportService {
//
//	private MutableRepository productCatalog;
//	private MutableRepository priceLists;
//
//	public MutableRepository getPriceLists() {
//		return priceLists;
//	}
//
//	public void setPriceLists(MutableRepository priceLists) {
//		this.priceLists = priceLists;
//	}
//
//	public AddSkusToProductsService() {
//		System.setProperty("http.proxyHost", "au-proxy.au.oracle.com");
//		System.setProperty("http.proxyPort", "80");
//	}
//
//	public MutableRepository getProductCatalog() {
//		return productCatalog;
//	}
//
//	public void setProductCatalog(MutableRepository productCatalog) {
//		this.productCatalog = productCatalog;
//	}
//
//	public void importUserData(Process pProcess, TransactionDemarcation pTD) throws Exception {
//		try {
//			List<MutableRepositoryItem> allPharmProducts = findAllPharm();
//
//			MutableRepositoryItem listPrices1005438 = createPriceListItem("1005438");
//			listPrices1005438.setPropertyValue("displayName", "Cardiff Pharmacy");
//			priceLists.addItem(listPrices1005438);
//			priceLists.updateItem(listPrices1005438);
//
//			MutableRepositoryItem listPrices1005446 = createPriceListItem("1005446");
//			listPrices1005446.setPropertyValue("displayName", "Jannali Chemmart");
//			priceLists.addItem(listPrices1005446);
//			priceLists.updateItem(listPrices1005446);
//			
//			
//			MutableRepositoryItem listPrices = (MutableRepositoryItem) priceLists.getItem("listPrices", "priceList");
//			MutableRepositoryItem salePrices = (MutableRepositoryItem) priceLists.getItem("salePrices", "priceList");
//
//			for (MutableRepositoryItem product : allPharmProducts) {
//				String skuId = "sku" + product.getRepositoryId();
//				MutableRepositoryItem sku = createOrGetSkuItem(skuId);
//				
//				
//				String p1 = (String) product.getPropertyValue("p1");
//				String p2 = (String) product.getPropertyValue("p2");
//				Double listPriceD = getPrice(p1);
//				Double salePriceD = getPrice(p2);
//
//				MutableRepositoryItem listPrice = createPriceItem("l." + product.getRepositoryId());
//				listPrice.setPropertyValue("priceList", listPrices);
//				listPrice.setPropertyValue("listPrice", listPriceD);
//				listPrice.setPropertyValue("skuId", skuId);
//				listPrice.setPropertyValue("pricingScheme", "listPrice");
//				priceLists.addItem(listPrice);
//				priceLists.updateItem(listPrice);
//
//				MutableRepositoryItem salePrice = createPriceItem("s." + product.getRepositoryId());
//				salePrice.setPropertyValue("priceList", salePrices);
//				salePrice.setPropertyValue("listPrice", salePriceD);
//				salePrice.setPropertyValue("skuId", skuId);
//				salePrice.setPropertyValue("pricingScheme", "listPrice");
//				priceLists.addItem(salePrice);
//				priceLists.updateItem(salePrice);
//
//				MutableRepositoryItem prices1005438 = createPriceItem(1005438 + "." + product.getRepositoryId());
//				prices1005438.setPropertyValue("priceList", listPrices1005438);
//				prices1005438.setPropertyValue("listPrice", round(salePriceD * .95, 2, BigDecimal.ROUND_HALF_UP));
//				prices1005438.setPropertyValue("skuId", skuId);
//				prices1005438.setPropertyValue("pricingScheme", "listPrice");
//				priceLists.addItem(prices1005438);
//				priceLists.updateItem(prices1005438);
//
//				sku.setPropertyValue("onSale", Boolean.TRUE);
//				sku.setPropertyValue("displayNameDefault", product.getPropertyValue("displayNameDefault"));
//
//				sku.setPropertyValue("displayNameDefault", product.getPropertyValue("displayNameDefault"));
//				MutableRepositoryItem template = (MutableRepositoryItem) productCatalog.getItem("xm1003", "media-external");
//				sku.setPropertyValue("template", template);
//
//				Set<MutableRepositoryItem> parentProducts = new HashSet<MutableRepositoryItem>();
//				parentProducts.add(product);
//				sku.setPropertyValue("parentProducts", parentProducts);
//
//				productCatalog.addItem(sku);
//				productCatalog.updateItem(sku);
//
//				List<MutableRepositoryItem> childSKUs = new ArrayList<MutableRepositoryItem>();
//				childSKUs.add(sku);
//				product.setPropertyValue("childSKUs", childSKUs);
//				product.setPropertyValue("template", template);
//				product.setPropertyValue("longDescriptionDefault", getLongDescription(product));
//				product.setPropertyValue("averageCustomerRating", averageUserRatings());
//
//				productCatalog.addItem(product);
//				productCatalog.updateItem(product);
//			}
//		} catch (Throwable t) {
//			t.printStackTrace();
//		}
//	}
//
//	
//	public static double round(double unrounded, int precision, int roundingMode)
//	{
//	    BigDecimal bd = new BigDecimal(unrounded);
//	    BigDecimal rounded = bd.setScale(precision, roundingMode);
//	    return rounded.doubleValue();
//	}
//	
//	private Double averageUserRatings() {
//		int Low = 100;
//		int High = 500;
//		Random r = new Random();
//		int theNumber = r.nextInt(High - Low) + Low;
//		Double result = ((double) theNumber / 100);
//		return result;
//	}
//
//	private String getLongDescription(MutableRepositoryItem product) {
//		if (product == null) {
//			return "";
//		}
//
//		String data = (String) product.getPropertyValue("indications");
//		if (data == null || data.isEmpty()) {
//			return "";
//		}
//		data = data.replaceAll("Description", "<span class='pharm_description'>Description</span>");
//		data = data.replaceAll("Warnings", "<span class='pharm_warnings'>Warnings</span>");
//		data = data.replaceAll("Directions", "<span class='pharm_directions'>Directions</span>");
//		data = data.replaceAll("Description", "<span class='pharm_ingredients'>Ingredients</span>");
//		data = data.replaceAll("Code", "<span class='pharm_code'>Code</span>");
//		return data;
//	}
//
//	private MutableRepositoryItem createOrGetSkuItem(String id) throws RepositoryException {
//		MutableRepositoryItem item = (MutableRepositoryItem) productCatalog.getItem(id, "sku");
//		if (item != null) {
//			item = productCatalog.getItemForUpdate(id, "sku");
//		} else {
//			item = productCatalog.createItem(id, "sku");
//		}
//
//		return item;
//	}
//
//	private MutableRepositoryItem createPriceListItem(String id) throws RepositoryException {
//		MutableRepositoryItem item = (MutableRepositoryItem) priceLists.getItem(id, "priceList");
//		if (item != null) {
//			item = priceLists.getItemForUpdate(id, "priceList");
//		} else {
//			item = priceLists.createItem(id, "priceList");
//		}
//		return item;
//	}
//
//	private MutableRepositoryItem createPriceItem(String id) throws RepositoryException {
//		MutableRepositoryItem item = (MutableRepositoryItem) priceLists.getItem(id, "price");
//		if (item != null) {
//			item = priceLists.getItemForUpdate(id, "price");
//		} else {
//			item = priceLists.createItem(id, "price");
//		}
//
//		return item;
//	}
//
//	private Double getPrice(String price) {
//		Double result = 0d;
//		if (price == null || price.isEmpty()) {
//			return result;
//		}
//		price = price.replaceAll("Was", "");
//		price = price.replaceAll("RRP", "");
//		price = price.replaceAll("AUD", "");
//		price = price.replaceAll("\\$", "");
//		price = price.trim();
//		try {
//			result = Double.parseDouble(price);
//		} catch (Exception e) {
//			e.printStackTrace();
//		}
//		return result;
//	}
//
//	private List<MutableRepositoryItem> findAllPharm() throws Exception {
//
//		List<MutableRepositoryItem> pharms = new ArrayList<MutableRepositoryItem>();
//		RepositoryView rview = productCatalog.getView("product");
//		RqlStatement statement = RqlStatement.parseRqlStatement("pharm = ?0");
//		Object params[] = new Object[1];
//		params[0] = new String("TRUE");
//		RepositoryItem[] items = statement.executeQuery(rview, params);
//		if (items != null && items.length != 0) {
//			for (int i = 0; i < items.length; i++) {
//				pharms.add((MutableRepositoryItem) items[i]);
//			}
//		}
//		return pharms;
//	}
//
//}
