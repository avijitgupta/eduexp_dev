//package com.importer;
//
//import java.io.File;
//import java.util.List;
//import java.util.concurrent.TimeUnit;
//
//import javax.xml.bind.JAXBContext;
//import javax.xml.bind.Unmarshaller;
//
//import org.openqa.selenium.By;
//import org.openqa.selenium.htmlunit.HtmlUnitDriver;
//import org.sitemaps.TUrl;
//import org.sitemaps.Urlset;
//
//import atg.dtm.TransactionDemarcation;
//import atg.epub.ProgramaticImportService;
//import atg.epub.project.Process;
//import atg.repository.MutableRepository;
//import atg.repository.MutableRepositoryItem;
//
//@SuppressWarnings({ "rawtypes", "unchecked" })
//public class CreateCategoriesService extends ProgramaticImportService {
//
//	private MutableRepository productCatalog;
//
//	private String filename = "";
//	
//	public String getFilename() {
//		return filename;
//	}
//
//	public void setFilename(String filename) {
//		this.filename = filename;
//	}
//
//	public CreateCategoriesService() {
//		System.setProperty("http.proxyHost", "au-proxy.au.oracle.com");
//		System.setProperty("http.proxyPort", "80");
//	}
//
//	public MutableRepository getProductCatalog() {
//		return productCatalog;
//	}
//
//	public void setProductCatalog(MutableRepository productCatalog) {
//		this.productCatalog = productCatalog;
//	}
//
//	public void importUserData(Process pProcess, TransactionDemarcation pTD) throws Exception {
//		System.out.println("ffff");
//		List<TUrl> allProducts = getAllProducts();
//		HtmlUnitDriver driver = new HtmlUnitDriver(false);
//		driver.setProxy("au-proxy.au.oracle.com", 80);
//		driver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);
//
//		
//		for (TUrl tUrl : allProducts) {
//			String url = tUrl.getLoc();
//			driver.get(url);
//			String pageSource = driver.getPageSource();
//			
//			//System.out.println(pageSource);
//			String productId = driver.findElement(By.xpath("//section[@id='product-grid']/section[3]/p/span[4]")).getText();
//			MutableRepositoryItem product = getOrCreateProduct(  productId);
//			
//			System.out.print("X");
//		}
//	}
//	
//	
//	private MutableRepositoryItem getOrCreateProduct(String productId) throws Exception {
//	 	MutableRepositoryItem product = (MutableRepositoryItem) productCatalog.getItem(productId, "product");
//		if (product == null) {
//			product = productCatalog.createItem(productId, "product");
//			product.setPropertyValue("displayNameDefault", "");
//			product = (MutableRepositoryItem) productCatalog.addItem(product);
//		}
//		return product;
//	}
//	
// 	
//	private List<TUrl>  getAllProducts() throws Exception {
//		JAXBContext jaxbContext = JAXBContext.newInstance(Urlset.class);
//		Unmarshaller jaxbUnmarshaller = jaxbContext.createUnmarshaller();
// 		File file = new File(filename);
//		Urlset urlset = (Urlset) jaxbUnmarshaller.unmarshal(file);
//		return urlset.getUrl();
// 
//	}
//	
// 
//	 
//}
