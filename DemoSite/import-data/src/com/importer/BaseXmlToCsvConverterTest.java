package com.importer;

import java.io.File;
import java.util.ArrayList;
import java.util.List;

import org.apache.commons.io.FileUtils;

public class BaseXmlToCsvConverterTest {

	public static void main(String[] args) throws Exception {
		try {
			BaseXmlToCsvConverterTest baseXmlToCsvConverter = new BaseXmlToCsvConverterTest();
			baseXmlToCsvConverter.runit();
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	private void runit() throws Exception {
		try {
			String filename = "/Users/adminuser/Documents/eclipse-ecomm/import-data/data/GoogleShopping_standard.xml";
			BaseXmlToCsvConverter converter = new BaseXmlToCsvConverter(filename);
			converter.convert();
			File infile = new File("/app/oracle/product/endeca/apps/google-data-feed/data/GoogleShopping_standard.xml.csv");
			File outfile = new File("/app/oracle/product/endeca/apps/google-data-feed/data/GoogleShopping_standard.xml.result.csv");
			List<String> lines = FileUtils.readLines(infile, "UTF-8");
			List<String> outlines = new ArrayList<String>();
			boolean firstLine = true;
			for (String line : lines) {
				String outLine = line;
				// if(firstLine) {
				outLine = outLine.replace("\"", "");
				// }
				firstLine = false;
				outlines.add(outLine);

			}
			FileUtils.writeLines(outfile, outlines, false);

		} catch (Exception e) {
			e.printStackTrace();
		}
	}

}
