package com.importer;

import static org.junit.Assert.fail;

import java.io.File;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.TimeUnit;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.Unmarshaller;

import org.apache.commons.io.FileUtils;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.openqa.selenium.Alert;
import org.openqa.selenium.By;
import org.openqa.selenium.NoSuchElementException;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.htmlunit.HtmlUnitDriver;
import org.sitemaps.TUrl;
import org.sitemaps.Urlset;

public class Import {
	private WebDriver driver;
	private String baseUrl;
	private boolean acceptNextAlert = true;
	private StringBuffer verificationErrors = new StringBuffer();

	@Before
	public void setUp() throws Exception {
	}

	// "No match found"

	private String dataFile = "/Users/adminuser/Documents/workspace-ecomm/ImportData/data/sitemap.xml";

	


	private List<TUrl>  getAllProducts() throws Exception {
		JAXBContext jaxbContext = JAXBContext.newInstance(Urlset.class);
		Unmarshaller jaxbUnmarshaller = jaxbContext.createUnmarshaller();
		File file = new File(dataFile);
		Urlset urlset = (Urlset) jaxbUnmarshaller.unmarshal(file);
		return urlset.getUrl();

	}
	
	
	
	
	@Test
	public void testExtract() throws Exception {
		try {

			List<TUrl> products = getAllProducts() ;
			 for (TUrl tUrl : products) {
				System.out.println(tUrl.getLoc());
			}

		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	private List<DataBean> getDataBeans() {
		List<DataBean> beans = null;
		try {
			List<String> parts = getParts();
			beans = new ArrayList<DataBean>();
			for (String part : parts) {
				DataBean bean = createBean(part);
				if (bean.getPde().isEmpty() && bean.getMnpn().isEmpty()) {

				} else {
					beans.add(bean);
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		return beans;
	}

	private List<String> getParts() {
		List<String> allParts = null;
		try {
			allParts = new ArrayList<String>();
			String data = FileUtils.readFileToString(new File(dataFile));
			String[] parts = data.split("###");
			for (int i = 0; i < parts.length; i++) {
				String part = parts[i];
				allParts.add(part);
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		return allParts;
	}

	private DataBean createBean(String data) {
		DataBean bean = new DataBean();
		String[] parts = data.split("\n");
		for (int i = 0; i < parts.length; i++) {
			String part = "";
			try {
				part = parts[i].split(":")[1].trim();
			} catch (Exception e) {
			}

			if (i == 0) {
				bean.setPde(part);
			}
			if (i == 1) {
				bean.setMnpn(part);
			}
			if (i == 2) {
				bean.setSize(part);
			}
			if (i == 3) {
				bean.setSupplier(part);
			}
			if (i == 4) {
				bean.setPromo(part);
			}
			if (i == 5) {
				bean.setHeading(part);
			}
			if (i == 6) {
				bean.setDescription(part);
			}
			if (i == 7) {
				bean.setImages(part);
			}
			if (i == 8) {
				bean.setDisclaimers(part);
			}
		}
		return bean;
	}

	@Test
	public void testExtract1() throws Exception {
		try {
			driver = new HtmlUnitDriver(false);
			baseUrl = "http://www.chemistdirect.com.au/";
			driver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);

			driver.get(baseUrl + "/");
			driver.findElement(By.id("sli_search_1")).clear();
			driver.findElement(By.id("sli_search_1")).sendKeys("Gamophen");
			driver.findElement(By.cssSelector("button.button")).click();
			driver.findElement(By.cssSelector("img.product-image")).click();

			WebElement imageElement = driver.findElement(By.cssSelector("p.product-image > img"));
			String imageName = imageElement.getAttribute("src");
			System.out.println(imageName);

			FileUtils.copyURLToFile(new URL(imageName), new File("/tmp/Gamophen.jpg"), 6000, 6000);

			WebElement priceElement = driver.findElement(By.className("price"));
			String price = priceElement.getText();
			System.out.println(price);

			WebElement productDescriptionElement = driver.findElement(By.className("product-description"));
			String productDescription = productDescriptionElement.getText();
			System.out.println(productDescription);

		} catch (Exception e) {
			e.printStackTrace();
		}
	}

//	@Test
//	public void testExtract2() throws Exception {
//		try {
//			List<String> parts = getParts();
//			driver = new HtmlUnitDriver(false);
//			baseUrl = "http://www.chempro.com.au/";
//			driver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);
//
//			driver.get(baseUrl + "/epages/shop.sf/en_AU/?ObjectPath=/Shops/shop/Products/9300673452481");
//			String pageSource = driver.getPageSource();
//			Pattern pattern = Pattern.compile(".*large.*'/WebRoot(.*)'");
//
//			Matcher matcher = pattern.matcher(pageSource);
//			while (matcher.find()) {
//
//				String imageName = matcher.group();
//				String image = imageName.substring("              large  : '".length(), imageName.length() - 1);
//				String fullUrl = baseUrl + image;
//				FileUtils.copyURLToFile(new URL(fullUrl), new File("/tmp/9300673452481.jpg"), 6000, 6000);
//			}
//
//		} catch (Exception e) {
//			e.printStackTrace();
//		}
//	}

	@After
	public void tearDown() throws Exception {
		driver.quit();
		String verificationErrorString = verificationErrors.toString();
		if (!"".equals(verificationErrorString)) {
			fail(verificationErrorString);
		}
	}

	private boolean isElementPresent(By by) {
		try {
			driver.findElement(by);
			return true;
		} catch (NoSuchElementException e) {
			return false;
		}
	}

	private String closeAlertAndGetItsText() {
		try {
			Alert alert = driver.switchTo().alert();
			if (acceptNextAlert) {
				alert.accept();
			} else {
				alert.dismiss();
			}
			return alert.getText();
		} finally {
			acceptNextAlert = true;
		}
	}
}
